from django.db import models
from django.contrib.auth.models import User

# vt yle User import ja FK

class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField(default=0)
    image = models.ImageField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='product',null=True,blank=True)

    def __str__(self):
        return self.name


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='cart',null=True,blank=True)
    create_at = models.CharField(max_length=100)

    def __str__(self):
        return self.user


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='order',null=True,blank=True)
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE)
    date_ordered = models.DateField()
    total_price = models.IntegerField(default=0)

    def __str__(self):
        return self.total_price


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name='products',null=True,blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='products',null=True,blank=True)
    quantity = models.IntegerField(default=0)

    def __str__(self):
        return self.quantity
