from django.views.generic import UpdateView, DeleteView, CreateView,ListView,DetailView
from django.urls import reverse_lazy
from .models import Product
from .forms import OrderForm, ProductForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from rest_framework import viewsets
from .serializers import ProductSerializer
from rest_framework import generics


# functional Based views
def home(request):
    products = Products.objects.order_by('-date_posted')
    context = {'products': products, 'featured_products': featured_products}
    return render(request, 'home.html', context)



# class based Views
class HomeView(ListView):
    model = Product
    template_name = 'home.html'
    context_object_name = 'products'
    ordering = ['name']




class ProductDetailView(LoginRequiredMixin, DetailView):
    model = Product
    template_name = 'products/product_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['orders'] = self.object.orders.all()
        context['product'] = self.object
        return context

    def product(self, request, *args, **kwargs):
        self.object = self.get_object()
        order_form = OrderForm(request.POST)
        if order_form.is_valid():
            order = order_form.save(commit=False)
            order.author = request.user
            order.post = self.object
            order.save()
            return redirect('order_detail', pk=self.object.id)
        context = self.get_context_data(object=self.object)
        context['order_form'] = order_form
        return self.render_to_response(context)


class ProductCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'products/product_form.html'
    success_url = reverse_lazy('home')
    permission_required = 'products.add_products'


class ProductUpdateView(LoginRequiredMixin,PermissionRequiredMixin, UpdateView):
    model = Product
    form_class = ProductForm
    template_name = 'products/product_form.html'
    success_url = reverse_lazy('home')
    permission_required = 'products.change_products'


class ProductDeleteView(LoginRequiredMixin, PermissionRequiredMixin,DeleteView):
    model = Product
    template_name = 'products/product_confirm_delete.html'
    success_url = reverse_lazy('home')
    permission_required = 'products.delete_products'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product = self.get_object()
        context['product'] = product
        return context

#*************** REST FRAMEWORK *******************


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

