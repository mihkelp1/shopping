
from rest_framework import serializers
from .models import Product , Order


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        exclude = ['user']   # You can specify fields explicitly if needed


class ProductSerializer(serializers.ModelSerializer):
    order = OrderSerializer(many=True, read_only=True)
    product_name = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = ['name', 'price','image', 'category']  # You can specify fields explicitly if needed

    def get_product_name(self, obj):
        return obj.product.name if obj.product else None




